=== Feedback Form Users ===
Contributors: shahinurislam
Donate link: https://forms.gle/EAtaCDDDxhcU5fva7
Tags: accessibility, feedback, contact, report form, survey
Requires at least: 5.9
Tested up to: 6.1
Stable tag: 1.0
Requires PHP: 7.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Just Install and survey from users and get feedback from users. Simple but flexible.

== Description ==

Install and survey from users and get feedback from users for WordPress. Take full control over your WordPress site, No need shortcode you can imagine – no programming knowledge required.

= Docs & Support =

You can find [docs](https://wordpress.org/plugins/feedback-form-users), [FAQ](https://wordpress.org/plugins/feedback-form-users) and more detailed information about Feedback Form Users on [gitlab](https://gitlab.com/shahinurislam/feedback-form-users). If you were unable to find the answer to your question on the FAQ or in any of the documentation, you should check the [support forum](https://wordpress.org/support/plugin/feedback-form-users) on WordPress.org. If you can't locate any topics that pertain to your particular issue, post a new topic for it.

== Why Use Feedback Form Users? ==

Beacause this plugin help to you improve your site and collect feedback from users.

https://youtu.be/fVhCiaFE6qw

== Feedback Form Users Needs Your Support ==

It is hard to continue development and support for this free plugin without contributions from users like you. If you enjoy using Feedback Form Users and find it useful, please consider [__making a donation__](https://forms.gle/EAtaCDDDxhcU5fva7). Your donation will help encourage and support the plugin's continued development and better user support. Find on gitlab. [Gitlab](https://gitlab.com/shahinurislam/feedback-form-users) 

== Installation ==

1. Upload the entire `Feedback Form Users` folder to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.

You will find 'Contact' menu in your WordPress admin panel.

For basic usage, you can also have a look at the [plugin web site](https://gitlab.com/shahinurislam/feedback-form-users).

== Frequently Asked Questions ==

Do you have questions or issues with Feedback Form Users? Use these support channels appropriately.

= Feedback Form Users is responsive? =

Yes this plugin are fully responsive and support every mobile device.

= What about this plugin? =

This plugin is collect feedback from users.

== Upgrade Notice ==

This is new version 1.0

== Screenshots ==

1. screenshot-1.png 
2. screenshot-2.png 
3. screenshot-3.png 

== Changelog ==

= 1.0 =

* New initialization.
