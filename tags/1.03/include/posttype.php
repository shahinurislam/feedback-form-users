<?php
//<-- custom post --> 
add_action( 'init', 'ffu_feedbackform_posttype' );
function ffu_feedbackform_posttype() {
 $labels = array(
  'name'               => _x( 'Feedback', 'post type general name', 'fbp' ),
  'singular_name'      => _x( 'Feedback', 'post type singular name', 'fbp' ),
  'menu_name'          => _x( 'Feedbacks', 'admin menu', 'fbp' ),
  'name_admin_bar'     => _x( 'Feedback', 'add new on admin bar', 'fbp' ),
  'add_new'            => _x( 'Add New', 'Feedback', 'fbp' ),
  'add_new_item'       => __( 'Add New Feedback', 'fbp' ),
  'new_item'           => __( 'New Feedback', 'fbp' ),
  'edit_item'          => __( 'Edit Feedback', 'fbp' ),
  'view_item'          => __( 'View Feedback', 'fbp' ),
  'all_items'          => __( 'All Feedbacks', 'fbp' ),
  'search_items'       => __( 'Search Feedbacks', 'fbp' ),
  'parent_item_colon'  => __( 'Parent Feedbacks:', 'fbp' ),
  'not_found'          => __( 'No Feedbacks found.', 'fbp' ),
  'not_found_in_trash' => __( 'No Feedbacks found in Trash.', 'fbp' )
 );
 $args = array(
  'labels'             => $labels,
  'description'        => __( 'Description.', 'fbp' ),
  'public'             => true,
  'publicly_queryable' => false,
  'show_ui'            => true,
  'show_in_menu'       => true,
  'query_var'          => true,
  'rewrite'            => array( 'slug' => 'feedbackform' ),
  'capability_type'    => 'post',
  'has_archive'        => true,
  'hierarchical'       => false,
  'menu_position'      => null,
  'supports'           => array( 'none')
 );
 register_post_type( 'feedbackform', $args );
} 
//remove add new button
function ffu_hide_feedbackform_add_new_button() {
    global $post_type;
    if ($post_type === 'feedbackform') {
        echo '<style>#menu-posts-feedbackform .wp-submenu li:nth-child(3), .post-type-feedbackform a.page-title-action { display: none !important; }</style>';
    }
    global $current_screen;
    if ($current_screen && $current_screen->post_type === 'feedbackform') {
        echo '<style>#wp-admin-bar-new-feedbackform { display: none !important; }</style>';
    }
}
add_action('admin_head', 'ffu_hide_feedbackform_add_new_button');
//show editor post
function ffu_custom_metabox_content() {
    global $post;
    $post_content = $post->post_content;
    $post_title = $post->post_title;
    ?>
    <div class="custom-title-metabox">
        <label for="custom_post_title">Title: <b><?php echo esc_attr($post_title); ?></b></label>
    </div>
    <div class="custom-content-metabox">
        <?php echo wpautop($post_content); ?>
    </div>
    <?php
}
function ffu_add_custom_metabox() {
    add_meta_box('ffu_custom_content_metabox', 'View Entrie', 'ffu_custom_metabox_content', 'feedbackform', 'normal'); // Replace 'post' with your custom post type if needed
}
add_action('add_meta_boxes', 'ffu_add_custom_metabox');
//change edit to view
function ffu_change_edit_text_to_view($translated_text, $untranslated_text, $domain) {
    global $post_type;
    if ($post_type === 'feedbackform') {
        // Change "Edit" to "View"
        if ($untranslated_text === 'Edit') {
            return 'View';
        }
    }
    // Return the original translation for other texts
    return $translated_text;
}
add_filter('gettext', 'ffu_change_edit_text_to_view', 10, 3);
?>
