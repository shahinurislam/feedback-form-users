<?php
/*
 * Plugin Name: Feedback Form Users
 * Plugin URI: https://wordpress.org/plugins/feedback-form-users
 * Description: Install and send your feedback.
 * Author: Md. Shahinur Islam
 * Author URI: https://profiles.wordpress.org/shahinurislam
 * Version: 1.01
 * Text Domain: ffu
 * Domain Path: /lang
 * Network: True
 * License: GPLv2
 * Requires at least: 5.9
 * Requires PHP: 7.4
 */
//--------------------- Create css and js ---------------------------//
define( 'FFU_PLUGIN', __FILE__ );
define( 'FFU_PLUGIN_DIR', untrailingslashit( dirname( FFU_PLUGIN ) ) );
require_once FFU_PLUGIN_DIR . '/include/enqueue.php';
require_once FFU_PLUGIN_DIR . '/include/posttype.php'; 
//-------------All post show------------//
function ffu_shortcode_wrapper($atts) {
ob_start();
//check all input fileds
 if(isset($_POST['rating'])) {
	if(trim($_POST['rating']) === '') { 
		$hasError = true;
	} else {
		$rating = sanitize_text_field('Rating: '.trim($_POST['rating']));
	}	
	if(trim($_POST['whatmade']) === '') { 
		$hasError = true;
	} else {
		$whatmade = sanitize_text_field(trim($_POST['whatmade']));
	}	 
	if(trim($_POST['messages']) === '') { 
		$hasError = true;
	} else {
		if(function_exists('stripslashes')) {
			$messages = sanitize_text_field(stripslashes(trim($_POST['messages'])));
		} else {
			$messages = sanitize_text_field(trim($_POST['messages']));
		}
	}
	if(!isset($hasError)) {		
		$body = "$rating \n\nWhat made: $whatmade \n\nComments: $messages ";
		// Create post object to save data
		$my_post = array(
			'post_title'    => wp_strip_all_tags( $rating ),
			'post_content'  => $body,
			'post_status'   => 'publish',
			'post_author'   => 1,
			'post_category' => array( 8,39 ),
			'post_type'		=> 'feedbackform'
		);
		// Insert the post into the database
		wp_insert_post( $my_post );
		?> 
		<div id="snackbar">Thanks for your feedback.</div>
		<script> 
		  var x = document.getElementById("snackbar");
		  x.className = "show";
		  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
		</script> 
<?php 	 
	} 
}
?>  
 <!-- partial:index.partial.html -->
<div id="feedback-form-wrapper">
  <div id="floating-icon">
    <button type="button" class="btnprimary btn btn-primary">
		<a href="#feedbackform" class="btn btn-primary">Feedback</a> 
    </button>
  </div>  
<div id="feedbackform" class="popup-containerffu">
  <div class="popup-contentffu">
    <a href="#" class="close">&times;</a>
    <h3>Feedback Form</h3>
     <form action="#" method="post">
          <div class="modal-body-ffu">
              <div class="form-group form-group-ffu">
                <label for="exampleFormControlTextarea1-ffu">
					<?php if(get_option('formone')){
						esc_html_e(get_option('formone'), 'ffu');
					}else{
						esc_html_e('How likely you would like to recommand us to your friends?', 'ffu');
					}; ?>
                 </label>
                <div class="rating-input-wrapper d-flex justify-content-between mt-2  rating-input-wrapper-ffu">
                  <label><input type="radio" name="rating" value="1"/><span class="border border-ffu rounded px-3 py-2">1</span></label>
                  <label><input type="radio" name="rating" value="2" /><span class="border border-ffu rounded px-3 py-2">2</span></label>
                  <label><input type="radio" name="rating" value="3" /><span class="border border-ffu rounded px-3 py-2">3</span></label>
                  <label><input type="radio" name="rating" value="4" /><span class="border border-ffu rounded px-3 py-2">4</span></label>
                  <label><input type="radio" name="rating" value="5" /><span class="border border-ffu rounded px-3 py-2">5</span></label>
                  <label><input type="radio" name="rating" value="6" /><span class="border border-ffu rounded px-3 py-2">6</span></label>
                  <label><input type="radio" name="rating" value="7" /><span class="border border-ffu rounded px-3 py-2">7</span></label>
                  <label><input type="radio" name="rating" value="8" /><span class="border border-ffu rounded px-3 py-2">8</span></label>
                  <label><input type="radio" name="rating" value="9" /><span class="border border-ffu rounded px-3 py-2">9</span></label>
                  <label><input type="radio" name="rating" value="10" /><span class="border border-ffu rounded px-3 py-2">10</span></label>
                </div>
                <div class="rating-labels d-flex justify-content-between mt-2  rating-labels-ffu">
                  <label>Very unlikely(0)</label> - 
                  <label>Very likely(10)</label>
                </div>
              </div>
              <div class="form-group form-group-ffu">
                <label for="input-one-ffu">
					<?php if(get_option('formtwo')){
						esc_html_e(get_option('formtwo'),'ffu');
					}else{
						esc_html_e('What made you leave us so early?','ffu');
					}; ?>
				</label>
                <input type="text" name="whatmade" class="form-control" id="input-one" placeholder="">
              </div>
              <div class="form-group form-group-ffu">
                <label for="input-two">
					<?php if(get_option('formtree')){
						esc_html_e(get_option('formtree'),'ffu');
					}else{
						esc_html_e('Would you like to say something?','ffu');
					}; ?>					
				</label>
                <textarea class="form-control" name="messages" id="input-two" rows="3"></textarea>
              </div> 
          </div>
          <div class="modal-footer mt-2"> 
			<button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
  </div>
</div> 
<?php   
 return ob_get_clean();
}
add_shortcode('ffu_feedbackform','ffu_shortcode_wrapper'); 
 
add_action('wp_footer', 'ffu_feedbackform_hook');
function ffu_feedbackform_hook(){
	//echo apply_filters( 'the_content','[ffu_feedbackform]');
	echo do_shortcode('[ffu_feedbackform]');
};
// Dashboard Front Show settings page
register_activation_hook(__FILE__, 'ffu_plugin_activate');
add_action('admin_init', 'ffu_plugin_redirect');
function ffu_plugin_activate() {
    add_option('ffu_plugin_do_activation_redirect', true);
}
function ffu_plugin_redirect() {
    if (get_option('ffu_plugin_do_activation_redirect', false)) {
        delete_option('ffu_plugin_do_activation_redirect');
        if(!isset($_GET['activate-multi']))
        {
            wp_redirect("edit.php?post_type=feedbackform&page=ffu_settings");
        }
    }
}
//side setting link
add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'ffu_plugin_action_links' );
function ffu_plugin_action_links( $actions ) {
   $actions[] = '<a href="'. esc_url( get_admin_url(null, 'edit.php?post_type=feedbackform&page=ffu_settings') ) .'">Settings</a>';
   $actions[] = '<a href="https://forms.gle/EAtaCDDDxhcU5fva7" target="_blank">Support for contact</a>';
   return $actions;
}
add_action('admin_menu', 'ffu_register_my_custom_submenu_page'); 
function ffu_register_my_custom_submenu_page() {
    add_submenu_page(
        'edit.php?post_type=feedbackform',
        'Settings',
        'Settings',
        'manage_options',
        'ffu_settings',
        'ffu_my_custom_submenu_page_callback' );
} 
function ffu_my_custom_submenu_page_callback() {
?><h1><?php esc_html_e( 'Welcome to Feedback Form.', 'ffu' ); ?></h1>  
<form method="post" action="options.php">
	<?php wp_nonce_field('update-options') ?>	
		<h3><strong><?php esc_html_e( 'Changes if you want.', 'ffu' );?></strong><br /></h3>
			<!-- partial:index.partial.html -->
			<section>
				<div>
					<label for="one"><span>How likely you would like to recommand us to your friends?</span>
					<input name="formone" type="text" id="one" value="<?php esc_html_e(get_option('formone'),'ffu'); ?>"></label>
				</div>
				<br /> 
				<div>
					<label for="two"><span>What made you leave us so early?</span>
					<input name="formtwo" type="text" id="two" value="<?php esc_html_e(get_option('formtwo'),'ffu'); ?>"></label>
				</div>
				<br /> 
				<div>
					<label for="two"><span>Would you like to say something?</span>
					<input name="formtree" type="text" id="two" value="<?php esc_html_e(get_option('formtree'),'ffu'); ?>"></label>
				</div>
			</section>
			<!-- partial -->
			<br />
			<br />
		<button class="button-ffu" role="button" type="submit">Save Settings</button>
		<input type="hidden" name="action" value="update" />
		<input type="hidden" name="page_options" value="formone, formtwo, formtree" /> 
</form>
<?php
}
