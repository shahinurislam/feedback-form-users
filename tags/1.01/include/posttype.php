<?php
//<-- custom post --> 
add_action( 'init', 'ffu_feedbackform_posttype' );
function ffu_feedbackform_posttype() {
 $labels = array(
  'name'               => _x( 'Feedback', 'post type general name', 'fbp' ),
  'singular_name'      => _x( 'Feedback', 'post type singular name', 'fbp' ),
  'menu_name'          => _x( 'Feedbacks', 'admin menu', 'fbp' ),
  'name_admin_bar'     => _x( 'Feedback', 'add new on admin bar', 'fbp' ),
  'add_new'            => _x( 'Add New', 'Feedback', 'fbp' ),
  'add_new_item'       => __( 'Add New Feedback', 'fbp' ),
  'new_item'           => __( 'New Feedback', 'fbp' ),
  'edit_item'          => __( 'Edit Feedback', 'fbp' ),
  'view_item'          => __( 'View Feedback', 'fbp' ),
  'all_items'          => __( 'All Feedbacks', 'fbp' ),
  'search_items'       => __( 'Search Feedbacks', 'fbp' ),
  'parent_item_colon'  => __( 'Parent Feedbacks:', 'fbp' ),
  'not_found'          => __( 'No Feedbacks found.', 'fbp' ),
  'not_found_in_trash' => __( 'No Feedbacks found in Trash.', 'fbp' )
 );
 $args = array(
  'labels'             => $labels,
  'description'        => __( 'Description.', 'fbp' ),
  'public'             => true,
  'publicly_queryable' => true,
  'show_ui'            => true,
  'show_in_menu'       => true,
  'query_var'          => true,
  'rewrite'            => array( 'slug' => 'feedbackform' ),
  'capability_type'    => 'post',
  'has_archive'        => true,
  'hierarchical'       => false,
  'menu_position'      => null,
  'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
 );
 register_post_type( 'feedbackform', $args );
} 
?>
