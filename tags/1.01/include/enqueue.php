<?php
//--------------------------- If Add Shortcode use any single page with design ------------- //
function ffu_add_css_js(){        
    wp_enqueue_style( 'fbpstyle.cs', plugin_dir_url(__FILE__) . '../css/style.css', array(), '1.0.0', 'all' ); 
    wp_enqueue_style( 'fbpbootstrap.cs', plugin_dir_url(__FILE__) . '../css/bootstrap.min.css', array(), '1.0.0', 'all' );  
}add_action('wp_enqueue_scripts','ffu_add_css_js');
function ffu_admin_enqueue($hook) {
   if ( 'feedbackform_page_ffu_settings' == $hook ) {  
      wp_enqueue_style('ffu-admin-css', plugin_dir_url( __FILE__ ). '../css/admin.css', array(), '1.0.0', 'all');
    }
  }add_action('admin_enqueue_scripts', 'ffu_admin_enqueue');
?>
